#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 29 14:45:58 2020

@author: dewidehem
Description: 
    L'application web codée récupère les données à partir des devis en pdf. Ces
    données sont stockées dans un système de base de données relationnelle.
    J'ai choisi la technologie Postgres car je la connais bien et que le temps
    était limité. Il y aura une table principale avec les informations du devis
    et des tables secondaire pour les travaux avec l'id du devis. IL y aura aussi
    une table de gestion des utilisateurs de l'application web.
    
    A partir des données récupérées, on va faire du machine learning pour 
    estimer un devis.
    L'idée est de classer les devis en sous groupes qui auront chacun un 
    barycentre. La première étape est de determiner le nombre optimal de sous-
    groupes. On réussit à le faire en maximisant le critère silhouette.
    Une fois le nombre de clusters fixé, on cherche à quel cluster appartient
    le devis à estimer et on renvoie le prix du barycentre.
    Cet algorithme s'appelle le K-mean clustering.
    Une deuxieme méthode consisterait à faire du kmean clustering sur des
    sous-classes de travaux et d'additionner le tout.
    
    L'application web permettra de faire ces calculs et d'avoir un rendu visuel.
    
    PARTIE BASE DE DONNEES:
        Il y aura une table principale avec les caractéristiques
        principales du devis. Il y aura une table par type de travaux avec
        l'id du devis. Une table utilisateur sera aussi crée pour le site web.
        
    ALGORITHME LECTURE PDF:
        
"""

from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
import numpy as np
from matplotlib import pyplot as plt
import math
from flask import (Flask, render_template, request, redirect, session)
import pandas as pd
import psycopg2
from sqlalchemy import create_engine
from random import random, randint, choice, uniform
import seaborn as sns


def compute_Kmeans(df, numberOfClusters):
    #Fonction qui prend en entrée une dataframe et le nombre de cluster et 
    #renvoie les centroids du kmean clustering et les labels
    kmeans = KMeans(n_clusters=numberOfClusters).fit(df)
    centroids = kmeans.cluster_centers_
    labels = kmeans.labels_
    
    return centroids, labels


def find_k(df):
    #Fonction qui effectue le k mean clustering pour chaque nombre de cluster k
    #et renvoie le k optimal qui maximise le critère silhouette
    n = 33
    max_S=-1
    kopt=0
    data_numpy = df.to_numpy()
    for k in range (2,n):
        centroids, labels = compute_Kmeans(df, k)
        S = silhouette_score(data_numpy, labels, metric='euclidean',
                             sample_size=None, random_state=None)
        print(S,k)
        if S > max_S:
            max_S = S
            kopt = k
    
    return kopt


def estimation(dataframe, centroids, variables_entree):
    #Fonction qui prend en entrée la dataframe, les centroids et les deux
    #surface et qui renvoie le prix de l'estimation
    surface_murale = variables_entree[0]
    surface_plafond = variables_entree[1]
    surface_sol = variables_entree[2]
    surface_pose_sol = variables_entree[3]
    unites_elec = variables_entree[4]
    unites = variables_entree[5]
    prix = 0
    dico_centroids = {}
    #Boucle d'initialisation des structures de données. Le dictionnaire
    #dico_centroids a pour clef les tuples de coordonnées des centroids et pour
    #valeurs les listes de chaque prix des devis de la classe correspondantes
    for centroid in centroids:
        centroid_liste = centroid.tolist()
        centroid_tuple = tuple(centroid_liste)
        dico_centroids[centroid_tuple] = []
    #Boucle qui remplie le dictionnaire dico_centroids en mettant chaque prix
    #de devis dans la liste du centroids le plus proche
    for x in dataframe.itertuples():
        minimum = math.inf
        for centroid in centroids:
            centroid_liste = centroid.tolist()
            centroid_tuple = tuple(centroid_liste)
            distance= math.sqrt((centroid_liste[0]-x.surface_murale)**2 +
                                (centroid_liste[1]-x.surface_plafond)**2 +
                                (centroid_liste[2]-x.surface_sol)**2 +
                                (centroid_liste[3]-x.surface_pose_sol)**2 +
                                (centroid_liste[4]-x.unites_elec)**2 +
                                (centroid_liste[5]-x.unites)**2)
            if distance < minimum:
                minimum = distance
                c = centroid_tuple
                prix = x.prix
        dico_centroids[c].append(prix)
    #Boucle qui calcule le centroids le plus proche du devis à estimer et
    #qui fait la moyenne des prix de la classe de ce centroid
    minimum=math.inf    
    for centroid in centroids:
        centroid_liste = centroid.tolist()
        centroid_tuple = tuple(centroid_liste)
        distance= math.sqrt((centroid_liste[0]-surface_murale)**2 +
                            (centroid_liste[0]-surface_plafond)**2 +
                            (centroid_liste[0]-surface_sol)**2 +
                            (centroid_liste[0]-surface_pose_sol)**2 +
                            (centroid_liste[0]-unites_elec)**2 +
                            (centroid_liste[1]-unites)**2)
        if distance < minimum:
            minimum = distance
            c = centroid_tuple
    array_prix = np.array(dico_centroids[c])
    return array_prix.mean()      


def plot_Kmean(df, numberOfClusters):
    #Fonction qui trace les points de chaque devis. En abscisse, on a la
    #surface plafond et en ordonnées la surface murale. Les centroids
    #de chaque classe sont aussi représentés
    centroids, labels = compute_Kmeans(df, numberOfClusters)
    plt.Figure(figsize=(4,3), dpi=100)
    plt.scatter(df['surface_murale'], df['surface_plafond'], c='green', s=50, alpha=0.5)
    plt.scatter(centroids[:, 0], centroids[:, 1], c='blue', s=50) 
    plt.xlabel("Surface murale en mètres carrés")
    plt.ylabel("Surface plafond en mètres carrés")
    plt.title("Surface plafond en fonction de la surface murale")
    plt.savefig("static/imgs/graphique1.png")
    plt.close()
    
    plt.Figure(figsize=(4,3), dpi=100)
    plt.scatter(df['surface_sol'], df['surface_pose_sol'], c='green', s=50, alpha=0.5)
    plt.scatter(centroids[:, 2], centroids[:, 3], c='blue', s=50) 
    plt.xlabel("Surface sol en mètres carrés")
    plt.ylabel("Surface pose sol en mètres carrés")
    plt.title("Surface pose sol en fonction de la surface sol")
    plt.savefig("static/imgs/graphique2.png")
    plt.close()
    
    plt.Figure(figsize=(4,3), dpi=100)
    plt.scatter(df['unites_elec'], df['unites'], c='green', s=50, alpha=0.5)
    plt.scatter(centroids[:, 4], centroids[:,5], c='blue', s=50) 
    plt.xlabel("Unites de travaux d'electricite")
    plt.ylabel("Unites de travaux divers")
    plt.title("Unites de travaux divers en fonction des unites de travaux d'electricite")
    plt.savefig("static/imgs/graphique3.png")
    plt.close()
    
    graphique = sns.scatterplot(df['surface_murale'], df['surface_plafond'])
    fig = graphique.get_figure()
    fig.savefig("output.png")
    

con1 = psycopg2.connect(host="127.0.0.1",
                    database="postgres",
                    user="postgres",
                    password="nicolas",
                    port=5432)
            

cur = con1.cursor()



app = Flask(__name__)

@app.route('/')   
def page_accueil():

    return render_template("index.html")

@app.route('/insertion')
def insertion():   
    return render_template("insertion.html")

@app.route('/insertion_resultat',methods=['GET','POST'])
def insertion_resultat():
    erreur = False
    doublon = False
    phrase = "Les données ont été insérées"
    try:
        resultat = request.form.to_dict()
        surface_murale = float(resultat['surface_murale'])
        prixht_mural = float(resultat['prixht_mural'])
        surface_plafond = float(resultat['surface_plafond'])
        prixht_plafond = float(resultat['prixht_plafond'])
        surface_sol = float(resultat['surface_sol'])
        prixht_sol = float(resultat['prixht_sol'])
        surface_pose_sol = float(resultat['surface_pose_sol'])
        prixht_pose_sol = float(resultat['prixht_pose_sol'])
        unites_elec = int(resultat['unites_elec'])
        prixht_elec = float(resultat['prixht_elec'])
        unites = int(resultat['unites'])
        prixht_divers = float(resultat['prixht_divers'])
        prixttc = float(resultat['prixttc'])
    except:
        erreur = True
    
    if erreur:
        phrase = "Les données n'ont pas été insérées"
    else:
        with con1:
            requete = """SELECT prix_devis.id as id,surface_murale, 
            peinture_murale.prixHT as
            prixHT_mural,surface_plafond,peinture_plafond.prixHT as
            prixHT_plafond, surface_sol,peinture_sol.prixHT as
            prixHT_sol,surface_pose_sol, pose_sol.prixHT as
            prixHT_pose_sol , unites_elec, electricite.prixHT as
            prixHT_elec, unites, divers.prixHT as
            prixHT_divers , prix as prixTTC
            FROM peinture_murale, peinture_plafond, peinture_sol, 
            pose_sol, electricite, divers, prix_devis 
            WHERE prix_devis.id = peinture_murale.id 
            AND prix_devis.id = peinture_plafond.id 
            AND prix_devis.id = peinture_sol.id 
            AND prix_devis.id = pose_sol.id 
            AND prix_devis.id = electricite.id 
            AND prix_devis.id = divers.id"""
            variables_devis= pd.read_sql_query(requete,con1)
            id_devis = 0
            for ligne in variables_devis.itertuples():
                if ligne.surface_murale == surface_murale \
                and ligne.prixht_mural == prixht_mural \
                and ligne.surface_plafond == surface_plafond \
                and ligne.prixht_plafond == prixht_plafond \
                and ligne.surface_sol == surface_sol \
                and ligne.prixht_sol == prixht_sol \
                and ligne.surface_pose_sol == surface_pose_sol \
                and ligne.prixht_pose_sol == prixht_pose_sol \
                and ligne.unites_elec == unites_elec \
                and ligne.prixht_elec == prixht_elec \
                and ligne.unites == unites \
                and ligne.prixht_divers == prixht_divers \
                and ligne.prixttc == prixttc:
                    doublon = True
                id_devis=int(ligne.id) + 1
            if doublon:
                phrase = "Les données n'ont pas été insérées"
            else:
                requete = "INSERT INTO prix_devis VALUES ('"
                requete += str(id_devis)
                requete += "','"
                requete += str(prixttc)
                requete += "')"
                cur.execute(requete)
                
                requete = "INSERT INTO peinture_murale VALUES ('"
                requete += str(id_devis)
                requete += "','"
                requete += str(surface_murale)
                requete += "','"
                requete += str(prixht_mural)
                requete += "')"
                cur.execute(requete)
                
                requete = "INSERT INTO peinture_plafond VALUES ('"
                requete += str(id_devis)
                requete += "','"
                requete += str(surface_plafond)
                requete += "','"
                requete += str(prixht_plafond)
                requete += "')"
                cur.execute(requete)
                
                requete = "INSERT INTO peinture_sol VALUES ('"
                requete += str(id_devis)
                requete += "','"
                requete += str(surface_sol)
                requete += "','"
                requete += str(prixht_sol)
                requete += "')"
                cur.execute(requete)
                
                requete = "INSERT INTO pose_sol VALUES ('"
                requete += str(id_devis)
                requete += "','"
                requete += str(surface_pose_sol)
                requete += "','"
                requete += str(prixht_pose_sol)
                requete += "')"
                cur.execute(requete)
                
                requete = "INSERT INTO electricite VALUES ('"
                requete += str(id_devis)
                requete += "','"
                requete += str(unites_elec)
                requete += "','"
                requete += str(prixht_elec)
                requete += "')"
                cur.execute(requete)
                
                requete = "INSERT INTO divers VALUES ('"
                requete += str(id_devis)
                requete += "','"
                requete += str(unites)
                requete += "','"
                requete += str(prixht_divers)
                requete += "')"
                cur.execute(requete)
                
                
                
    
    return render_template("insertion_resultat.html",phrase = phrase)

@app.route('/sauvegarde')
def sauvegarde():
        
    with con1:
        requete="select * from prix_devis"
        prix_devis = pd.read_sql_query(requete,con1)
        prix_devis.to_csv("prix_devis.csv",index = False)
        
        requete="select * from peinture_murale"
        peinture_murale = pd.read_sql_query(requete,con1)
        peinture_murale.to_csv("peinture_murale.csv",index = False)
        
        requete="select * from peinture_plafond"
        peinture_plafond = pd.read_sql_query(requete,con1)
        peinture_plafond.to_csv("peinture_plafond.csv",index = False)
        
        requete="select * from peinture_sol"
        peinture_sol = pd.read_sql_query(requete,con1)
        peinture_sol.to_csv("peinture_sol.csv",index = False)
        
        requete="select * from pose_sol"
        pose_sol = pd.read_sql_query(requete,con1)
        pose_sol.to_csv("pose_sol.csv",index = False)
        
        requete="select * from electricite"
        electricite = pd.read_sql_query(requete,con1)
        electricite.to_csv("electricite.csv",index = False)
        
        requete="select * from divers"
        divers = pd.read_sql_query(requete,con1)
        divers.to_csv("divers.csv",index = False)
        
    return render_template("sauvegarde.html")

@app.route('/chargement')
def chargement():
    engine = create_engine('postgresql://postgres:nicolas@127.0.0.1:5432/')
    con = engine.connect()
    
    prix_devis = pd.read_csv("prix_devis.csv")
    peinture_murale = pd.read_csv("peinture_murale.csv")
    peinture_plafond = pd.read_csv("peinture_plafond.csv")
    peinture_sol = pd.read_csv("peinture_sol.csv")
    pose_sol = pd.read_csv("pose_sol.csv")
    electricite = pd.read_csv("electricite.csv")
    divers = pd.read_csv("divers.csv")
    
    with con:
        peinture_murale.to_sql('peinture_murale', engine, if_exists='replace',index=False)
        peinture_plafond.to_sql('peinture_plafond', engine, if_exists='replace',index=False)
        peinture_sol.to_sql('peinture_sol', engine, if_exists='replace',index=False)
        pose_sol.to_sql('pose_sol', engine, if_exists='replace',index=False)
        electricite.to_sql('electricite', engine, if_exists='replace',index=False)
        divers.to_sql('divers', engine, if_exists='replace',index=False)
        prix_devis.to_sql('prix_devis', engine, if_exists='replace',index=False)
    
    return render_template("chargement.html")

@app.route('/reinitialisation')
def reinitialisation():
    engine = create_engine('postgresql://postgres:nicolas@127.0.0.1:5432/')
    con = engine.connect()

    prix_devis=pd.DataFrame(columns= ['id','prix'])    
    peinture_murale=pd.DataFrame(columns= ['id','surface_murale','prixht'])
    peinture_plafond=pd.DataFrame(columns= ['id','surface_plafond','prixht'])
    peinture_sol=pd.DataFrame(columns= ['id','surface_sol','prixht'])
    pose_sol=pd.DataFrame(columns= ['id','surface_pose_sol','prixht'])
    electricite=pd.DataFrame(columns= ['id','unites_elec','prixht'])
    divers=pd.DataFrame(columns= ['id','unites','prixht'])
    
    #Mise des dataframes dans les tables
    with con:
        peinture_murale.to_sql('peinture_murale', engine, if_exists='replace',index=False)
        peinture_plafond.to_sql('peinture_plafond', engine, if_exists='replace',index=False)
        peinture_sol.to_sql('peinture_sol', engine, if_exists='replace',index=False)
        pose_sol.to_sql('pose_sol', engine, if_exists='replace',index=False)
        electricite.to_sql('electricite', engine, if_exists='replace',index=False)
        divers.to_sql('divers', engine, if_exists='replace',index=False)
        prix_devis.to_sql('prix_devis', engine, if_exists='replace',index=False)
    
    return render_template('reinitialisation.html')

@app.route('/generation_aleatoire_formulaire')
def generation_aleatoire_formulaire():
    
    return render_template("generation_aleatoire_formulaire.html")

@app.route('/generation_aleatoire',methods=['GET','POST'])
def generation_aleatoire():
    resultat = request.form.to_dict()
    
    engine = create_engine('postgresql://postgres:nicolas@127.0.0.1:5432/')
    con = engine.connect()

    prix_devis=pd.DataFrame(columns= ['id','prix'])    
    peinture_murale=pd.DataFrame(columns= ['id','surface_murale','prixht'])
    peinture_plafond=pd.DataFrame(columns= ['id','surface_plafond','prixht'])
    peinture_sol=pd.DataFrame(columns= ['id','surface_sol','prixht'])
    pose_sol=pd.DataFrame(columns= ['id','surface_pose_sol','prixht'])
    electricite=pd.DataFrame(columns= ['id','unites_elec','prixht'])
    divers=pd.DataFrame(columns= ['id','unites','prixht'])
    
    nombre_devis=int(resultat['ndevis'])
    idevis = 0
    for numero_devis in range(nombre_devis):
        #peinture murale
        peinture_m = choice([True, False])
        if peinture_m:
            peinture_murale.loc[idevis,'id'] = numero_devis
            surface = uniform(10,500)
            peinture_murale.loc[idevis,'surface_murale'] = surface
            prix = surface * uniform(30,50)
            peinture_murale.loc[idevis,'prixht'] = prix
        else:
            peinture_murale.loc[idevis,'id'] = numero_devis
            peinture_murale.loc[idevis,'surface_murale'] = 0.0
            peinture_murale.loc[idevis,'prixht'] = 0.0
            #peinture plafond
        peinture_p = choice([True, False])
        if peinture_p:
            peinture_plafond.loc[idevis,'id'] = numero_devis
            surface = uniform(5,100)
            peinture_plafond.loc[idevis,'surface_plafond'] = surface
            prix = surface * uniform(30,50)
            peinture_plafond.loc[idevis,'prixht'] = prix
        else:
            peinture_plafond.loc[idevis,'id'] = numero_devis
            peinture_plafond.loc[idevis,'surface_plafond'] = 0.0
            peinture_plafond.loc[idevis,'prixht'] = 0.0
        
        #peinture sol
        peinture_s = choice([True, False])
        if peinture_s:
            peinture_sol.loc[idevis,'id'] = numero_devis
            surface = uniform(5,100)
            peinture_sol.loc[idevis,'surface_sol'] = surface
            prix = surface * uniform(30,50)
            peinture_sol.loc[idevis,'prixht'] = prix
        else:
            peinture_sol.loc[idevis,'id'] = numero_devis
            peinture_sol.loc[idevis,'surface_sol'] = 0.0
            peinture_sol.loc[idevis,'prixht'] = 0.0
            #pose sol
        pose_s = choice([True, False])
        if pose_s:
            pose_sol.loc[idevis,'id'] = numero_devis
            surface = uniform(5,100)
            pose_sol.loc[idevis,'surface_pose_sol'] = surface
            prix = surface * uniform(30,50)
            pose_sol.loc[idevis,'prixht'] = prix
        else:
            pose_sol.loc[idevis,'id'] = numero_devis
            pose_sol.loc[idevis,'surface_pose_sol'] = 0.0
            pose_sol.loc[idevis,'prixht'] = 0.0
            #electricite
        elec=choice([True, False])
        if elec:
            electricite.loc[idevis,'id'] = numero_devis
            unites = float(randint(1,50))
            electricite.loc[idevis,'unites_elec'] = unites
            prix = unites * uniform(50,1500)
            electricite.loc[idevis,'prixht'] = prix
        else:
            electricite.loc[idevis,'id'] = numero_devis
            electricite.loc[idevis,'unites_elec'] = 0.0
            electricite.loc[idevis,'prixht'] = 0.0
            #divers
        div=choice([True, False])
        if div:
            divers.loc[idevis,'id'] = numero_devis
            unites = float(randint(1,15))
            divers.loc[idevis,'unites'] = unites
            prix = unites * uniform(100,5000)
            divers.loc[idevis,'prixht'] = prix
        else:
            divers.loc[idevis,'id'] = numero_devis
            divers.loc[idevis,'unites'] = 0.0
            divers.loc[idevis,'prixht'] = 0.0
            #total
        prix1 = peinture_murale.loc[idevis,'prixht']
        prix2 = peinture_plafond.loc[idevis,'prixht']
        prix3 = peinture_sol.loc[idevis,'prixht']
        prix4 = pose_sol.loc[idevis,'prixht']
        prix5 = electricite.loc[idevis,'prixht']
        prix6 = divers.loc[idevis,'prixht']
        prix_devis.loc[idevis,'id'] = numero_devis
        prix_total = prix1 + prix2 + prix3 + prix4 + prix5 + prix6
        prix_devis.loc[idevis,'prix'] = prix_total*1.10
        idevis += 1
    
    #Mise des dataframes dans les tables
    with con:
        peinture_murale.to_sql('peinture_murale', engine, if_exists='replace',index=False)
        peinture_plafond.to_sql('peinture_plafond', engine, if_exists='replace',index=False)
        peinture_sol.to_sql('peinture_sol', engine, if_exists='replace',index=False)
        pose_sol.to_sql('pose_sol', engine, if_exists='replace',index=False)
        electricite.to_sql('electricite', engine, if_exists='replace',index=False)
        divers.to_sql('divers', engine, if_exists='replace',index=False)
        prix_devis.to_sql('prix_devis', engine, if_exists='replace',index=False)
        
    return render_template('generation_aleatoire.html')

@app.route('/visualisation_devis')
def visualisation_devis():
    categories=['prix_devis','peinture_murale','peinture_plafond',
                'peinture_sol','pose_sol','electricite','divers']
    
    return render_template('visualisation_devis.html',categories=categories)

@app.route('/visualisation_resultat',methods=['GET','POST'])
def visualisation_resultat():
    resultat = request.form.to_dict()
    texte = resultat['categorie']
    
    requete = "select * from " + texte
    with con1:
        cur.execute(requete)
        devis = pd.read_sql_query(requete,con1)
    
    
    return render_template('visualisation_resultat.html',
                           tables=[devis.to_html(classes='data')],
                           titles=devis.columns.values)




@app.route('/kmean_formulaire')
def kmean_formulaire():
    return render_template('kmean_formulaire.html')

@app.route('/resultat',methods = ['POST'])
def resultat():
    #Les différentes fonctions précédentes sont utilisées et les résultats
    #sont affichés dans la page résultat
    result = request.form.to_dict()
    variables_entree = []
    variables_entree.append(float(result['surface_murale']))
    variables_entree.append(float(result['surface_plafond']))
    variables_entree.append(float(result['surface_sol']))
    variables_entree.append(float(result['surface_pose_sol']))
    variables_entree.append(float(result['unites_elec']))
    variables_entree.append(float(result['unites']))
    
    requete = """select surface_murale, surface_plafond, surface_sol,
            surface_pose_sol, unites_elec, unites, prix
            from peinture_murale, peinture_plafond, peinture_sol, 
            pose_sol, electricite, divers, prix_devis 
            where prix_devis.id = peinture_murale.id 
            and prix_devis.id = peinture_plafond.id 
            and prix_devis.id = peinture_sol.id 
            and prix_devis.id = pose_sol.id 
            and prix_devis.id = electricite.id 
            and prix_devis.id = divers.id"""
            
    with con1:
        cur.execute(requete)
        variables_devis= pd.read_sql_query(requete,con1)
        variables_devis_kmean = pd.read_sql_query(requete,con1)
    del variables_devis_kmean['prix']

    k = find_k(variables_devis_kmean)
    centroids, labels = compute_Kmeans(variables_devis_kmean,k)
    print(centroids)
    prix = estimation(variables_devis, centroids, variables_entree)
    
    plot_Kmean(variables_devis_kmean,k)
    
    return render_template("resultat.html",prix=prix)





### Lancement de l'application 
if __name__ == '__main__':        
    app.run (debug = True, use_reloader=False)
        