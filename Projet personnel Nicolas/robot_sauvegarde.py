#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  3 09:44:14 2020

@author: dewidehem
"""

import pandas as pd
import psycopg2
import datetime
import os
import time



def sauvegarde_base_de_donnees():
    # listes des feuilles excel et des tables
    sheet=[]
    tb=[]
    
    # connexion db
    con = psycopg2.connect(host="127.0.0.1",
                    database="postgres",
                    user="postgres",
                    password="nicolas",
                    port=5432)
            

    cursorObj = con.cursor()
    
    # requête sql pour récupérer toutes les tables de la db
    cursorObj.execute("""SELECT table_name FROM information_schema.tables
                      WHERE table_schema='public'
                      AND table_type='BASE TABLE'""")
    #stocker dans tables la liste des tables dans .db
    tables=cursorObj.fetchall()
    
    # chaque table de la base de données est sauvegardée sous forme d'un 
    # fichier Excel  
    for table in tables:
        # Date actuelle 
        now = datetime.datetime.now()
        # date actuelle en chaine de carac
        str_now = now.strftime("%Y-%m-%d-%H-%M-%S")
        with con:
            # onglet prend la valeur d'index 0 du tuple (le nom des tables)
            onglet = table[0]
            # on ajoute à la feuille excel la valeur de "onglet", 1ere itération
            # onglet : connaissances
            sheet.append(onglet)

            # on récupére toutes les données de "onglet"
            requete_SQL = "SELECT * FROM " +str(onglet)
            table_tb = pd.read_sql(requete_SQL, con)
            #on ajoute à la liste tb[] 
            tb.append(table_tb)
        nom_fichier = "BDD"+ str_now +".xlsx"

    with pd.ExcelWriter(nom_fichier) as writer:
        for i in range (0,len(sheet)) :
            tb[i].to_excel(writer, sheet_name=sheet[i], index=False )
        print(table_tb.head())
        print("C'est sauvegardé à "+str_now+" dans "+nom_fichier)
        
def rechargement(fichier):
        # connexion à la bdd
        con = psycopg2.connect(host="127.0.0.1",
                    database="postgres",
                    user="postgres",
                    password="nicolas",
                    port=5432)
        xl = pd.ExcelFile(fichier)
        
        # pour tous les noms de tables récupérés par xl.sheet_names
        for nom_table, i in zip(xl.sheet_names, range(len(xl.sheet_names))):
            # data frame du fichier excel
            df = pd.read_excel(fichier, sheet_name = i, header=0, 
                               index_col=0, keep_default_na=True)
            # on récupère les données de la df pour transfert en sql
            df.to_sql(nom_table, con, if_exists = 'replace')
            
        # fermeture de la connexion
        con.close()

    
    
    
while True:
    sauvegarde_base_de_donnees()
    time.sleep(3600000)