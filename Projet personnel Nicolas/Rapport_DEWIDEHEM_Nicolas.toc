\babel@toc {french}{}
\contentsline {chapter}{\numberline {1}Introduction}{2}% 
\contentsline {chapter}{\numberline {2}Premi\IeC {\`e}re partie}{3}% 
\contentsline {section}{\numberline {2.1}Compr\IeC {\'e}hension des besoins du client}{3}% 
\contentsline {section}{\numberline {2.2}Etat de l'art}{3}% 
\contentsline {subsection}{\numberline {2.2.1}Les diff\IeC {\'e}rents types de bases de donn\IeC {\'e}es}{3}% 
\contentsline {subsection}{\numberline {2.2.2}Les diff\IeC {\'e}rents langages}{3}% 
\contentsline {subsection}{\numberline {2.2.3}Les technologies web}{3}% 
\contentsline {section}{\numberline {2.3}Traduction et choix techniques du projet}{3}% 
\contentsline {subsection}{\numberline {2.3.1}La Base de Donn\IeC {\'e}es}{3}% 
\contentsline {subsection}{\numberline {2.3.2}Le langage Python et ses biblioth\IeC {\`e}ques}{3}% 
\contentsline {subsection}{\numberline {2.3.3}Html, Css, Jinja}{3}% 
\contentsline {subsection}{\numberline {2.3.4}L'application WEB}{3}% 
\contentsline {subsubsection}{G\IeC {\'e}n\IeC {\'e}ration al\IeC {\'e}atoire des donn\IeC {\'e}es}{3}% 
\contentsline {subsubsection}{Insertion des donn\IeC {\'e}es}{3}% 
\contentsline {subsubsection}{Visualisation des donn\IeC {\'e}es}{3}% 
\contentsline {subsubsection}{R\IeC {\'e}initialisation, sauvegardes et chargement}{3}% 
\contentsline {subsubsection}{Estimation et Machine Learning}{3}% 
\contentsline {chapter}{\numberline {3}Deuxi\IeC {\`e}me partie}{4}% 
\contentsline {section}{\numberline {3.1}Gestion de projet}{4}% 
\contentsline {section}{\numberline {3.2}Retours d'exp\IeC {\'e}rience sur les outils et techniques}{4}% 
\contentsline {chapter}{\numberline {4}Bilan et am\IeC {\'e}liorations}{5}% 
\contentsline {chapter}{\numberline {5}Conclusion}{6}% 
