#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 10:50:39 2020

@author: dewidehem
"""

import pandas as pd
from random import random, randint, choice, uniform
from sqlalchemy import create_engine
import psycopg2

engine = create_engine('postgresql://postgres:nicolas@127.0.0.1:5432/')
con = engine.connect()

prix_devis=pd.DataFrame(columns= ['id','prix'])    
peinture_murale=pd.DataFrame(columns= ['id','surface_murale','prixHT'])
peinture_plafond=pd.DataFrame(columns= ['id','surface_plafond','prixHT'])
peinture_sol=pd.DataFrame(columns= ['id','surface_sol','prixHT'])
pose_sol=pd.DataFrame(columns= ['id','surface_pose_sol','prixHT'])
electricite=pd.DataFrame(columns= ['id','unites_elec','prixHT'])
divers=pd.DataFrame(columns= ['id','unites','prixHT'])

nombre_devis=40
idevis = 0
for numero_devis in range(nombre_devis):
    #peinture murale
    peinture_m = choice([True, False])
    if peinture_m:
        peinture_murale.loc[idevis,'id'] = numero_devis
        surface = uniform(10,500)
        peinture_murale.loc[idevis,'surface_murale'] = surface
        prix = surface * uniform(30,50)
        peinture_murale.loc[idevis,'prixHT'] = prix
    else:
        peinture_murale.loc[idevis,'id'] = numero_devis
        peinture_murale.loc[idevis,'surface_murale'] = 0.0
        peinture_murale.loc[idevis,'prixHT'] = 0.0
    #peinture plafond
    peinture_p = choice([True, False])
    if peinture_p:
        peinture_plafond.loc[idevis,'id'] = numero_devis
        surface = uniform(5,100)
        peinture_plafond.loc[idevis,'surface_plafond'] = surface
        prix = surface * uniform(30,50)
        peinture_plafond.loc[idevis,'prixHT'] = prix
    else:
        peinture_plafond.loc[idevis,'id'] = numero_devis
        peinture_plafond.loc[idevis,'surface_plafond'] = 0.0
        peinture_plafond.loc[idevis,'prixHT'] = 0.0
        
    #peinture sol
    peinture_s = choice([True, False])
    if peinture_s:
        peinture_sol.loc[idevis,'id'] = numero_devis
        surface = uniform(5,100)
        peinture_sol.loc[idevis,'surface_sol'] = surface
        prix = surface * uniform(30,50)
        peinture_sol.loc[idevis,'prixHT'] = prix
    else:
        peinture_sol.loc[idevis,'id'] = numero_devis
        peinture_sol.loc[idevis,'surface_sol'] = 0.0
        peinture_sol.loc[idevis,'prixHT'] = 0.0
    #pose sol
    pose_s = choice([True, False])
    if pose_s:
        pose_sol.loc[idevis,'id'] = numero_devis
        surface = uniform(5,100)
        pose_sol.loc[idevis,'surface_pose_sol'] = surface
        prix = surface * uniform(30,50)
        pose_sol.loc[idevis,'prixHT'] = prix
    else:
        pose_sol.loc[idevis,'id'] = numero_devis
        pose_sol.loc[idevis,'surface_pose_sol'] = 0.0
        pose_sol.loc[idevis,'prixHT'] = 0.0
    #electricite
    elec=choice([True, False])
    if elec:
        electricite.loc[idevis,'id'] = numero_devis
        unites = float(randint(1,50))
        electricite.loc[idevis,'unites_elec'] = unites
        prix = unites * uniform(50,1500)
        electricite.loc[idevis,'prixHT'] = prix
    else:
        electricite.loc[idevis,'id'] = numero_devis
        electricite.loc[idevis,'unites_elec'] = 0.0
        electricite.loc[idevis,'prixHT'] = 0.0
    #divers
    div=choice([True, False])
    if div:
        divers.loc[idevis,'id'] = numero_devis
        unites = float(randint(1,15))
        divers.loc[idevis,'unites'] = unites
        prix = unites * uniform(100,5000)
        divers.loc[idevis,'prixHT'] = prix
    else:
        divers.loc[idevis,'id'] = numero_devis
        divers.loc[idevis,'unites'] = 0.0
        divers.loc[idevis,'prixHT'] = 0.0
    #total
    prix1 = peinture_murale.loc[idevis,'prixHT']
    prix2 = peinture_plafond.loc[idevis,'prixHT']
    prix3 = peinture_sol.loc[idevis,'prixHT']
    prix4 = pose_sol.loc[idevis,'prixHT']
    prix5 = electricite.loc[idevis,'prixHT']
    prix6 = divers.loc[idevis,'prixHT']
    prix_devis.loc[idevis,'id'] = numero_devis
    prix_total = prix1 + prix2 + prix3 + prix4 + prix5 + prix6
    prix_devis.loc[idevis,'prix'] = prix_total*1.10
    idevis += 1
    
#Mise des dataframes dans les tables
with con:
    peinture_murale.to_sql('peinture_murale', engine, if_exists='replace',index=False)
    peinture_plafond.to_sql('peinture_plafond', engine, if_exists='replace',index=False)
    peinture_sol.to_sql('peinture_sol', engine, if_exists='replace',index=False)
    pose_sol.to_sql('pose_sol', engine, if_exists='replace',index=False)
    electricite.to_sql('electricite', engine, if_exists='replace',index=False)
    divers.to_sql('divers', engine, if_exists='replace',index=False)
    prix_devis.to_sql('prix_devis', engine, if_exists='replace',index=False)